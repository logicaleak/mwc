package job

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"mwc/request"
	"net/http"
	"net/url"
	"testing"
)

func TestJob_InferLinks_Succeeds(t *testing.T) {
	h := `
		<!DOCTYPE html>
		<html>
			<head></head>
			<body>
				<a href="http://www.a.com/link1"></a>
                <a href="http://www.a.com/link2"></a>
				<div>
					<a href="http://www.a.com/link3"></a>
				</div>
			</body>
		</html>
	`
	parsedBase, err := url.Parse("http://www.a.com")
	assert.Nil(t, err)
	j := NewJob(1, parsedBase, nil)

	reader := bytes.NewReader([]byte(h))
	links, err := j.inferLinks(nil, reader)
	assert.Nil(t, err)

	url1, _ := url.Parse("http://www.a.com/link1")
	url2, _ := url.Parse("http://www.a.com/link2")
	url3, _ := url.Parse("http://www.a.com/link3")

	assert.Equal(t, []link{
		{
			url: url1,
		},
		{
			url: url2,
		},
		{
			url: url3,
		},
	}, links)
}

func TestSanitizeLink_WithRoot_Succeeds(t *testing.T) {
	parent, err := url.Parse("https://www.monzo.com")
	assert.Nil(t, err)

	j := NewJob(1, parent, nil)
	r := j.sanitizeLink(parent, "/")
	assert.Equal(t, "https://www.monzo.com", r)
}

func TestSanitizeLink_WithPureBase_WithSlashHref_Succeeds(t *testing.T) {
	parent, err := url.Parse("https://www.monzo.com")
	assert.Nil(t, err)

	j := NewJob(1, parent, nil)
	r := j.sanitizeLink(parent, "/hello")
	assert.Equal(t, "https://www.monzo.com/hello", r)
}

func TestSanitizeLink_WithSlashedBase_WithSlashHref_Succeeds(t *testing.T) {
	parent, err := url.Parse("https://www.monzo.com/")
	assert.Nil(t, err)

	j := NewJob(1, parent, nil)
	r := j.sanitizeLink(parent, "/hello")
	assert.Equal(t, "https://www.monzo.com/hello", r)
}

func TestSanitizeLink_WithPureBase_WithPureHref_Succeeds(t *testing.T) {
	parent, err := url.Parse("https://www.monzo.com")
	assert.Nil(t, err)

	j := NewJob(1, parent, nil)
	r := j.sanitizeLink(parent, "hello/path")
	assert.Equal(t, "https://www.monzo.com/hello/path", r)
}

func TestVisitor_WithRedirect_Succeeds(t *testing.T) {
	parsedBase, err := url.Parse("http://www.a.com")
	assert.Nil(t, err)
	requester := &request.MockRequester{}
	j := NewJob(1, parsedBase, requester)

	dc := make(chan link)
	cc := make(chan visitedLink)
	j.demandChan = dc
	j.collectorChan = cc

	go func() {
		dc <- link{
			url:    parsedBase,
			parent: nil,
		}
	}()

	requester.On("Get", mock.Anything, parsedBase).Return(&http.Response{
		StatusCode: http.StatusMovedPermanently,
		Header: http.Header{
			"Location": []string{"http://www.b.com"},
		},
	}, nil)

	go func() {
		j.visitor()
	}()
	visitedLink := <-cc

	assert.Nil(t, visitedLink.fatalErr)
	assert.Equal(t, 1, len(visitedLink.children))

	redirected, err := url.Parse("http://www.b.com")
	assert.Nil(t, err)
	assert.Equal(t, redirected, visitedLink.children[0].url)
}

func TestVisitor_WithError_Succeeds(t *testing.T) {
	parsedBase, err := url.Parse("http://www.a.com")
	assert.Nil(t, err)
	requester := &request.MockRequester{}
	j := NewJob(1, parsedBase, requester)

	dc := make(chan link)
	cc := make(chan visitedLink)
	j.demandChan = dc
	j.collectorChan = cc

	go func() {
		dc <- link{
			url:    parsedBase,
			parent: nil,
		}
	}()

	requester.On("Get", mock.Anything, parsedBase).Return(&http.Response{
		StatusCode: http.StatusInternalServerError,
	}, nil)

	go func() {
		j.visitor()
	}()
	visitedLink := <-cc

	assert.Nil(t, visitedLink.fatalErr)
	assert.Equal(t, 0, len(visitedLink.children))

}
