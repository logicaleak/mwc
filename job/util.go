package job

func minInt(ints...int) int {
	min := 0
	for i, val := range ints {
		if i == 0 {
			min = val
		} else {
			if val < min {
				min = val
			}
		}
	}
	return min
}
