package job

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io"
	"mwc/filter"
	"mwc/request"
	"net/http"
	"net/url"
	"path"
	"reflect"
	"runtime/debug"
	"strings"
	"sync/atomic"
	"time"
)

type link struct {
	// parent of this link
	parent *url.URL
	// url of this link
	url *url.URL
}

// visitedLink is returned as a result of a visit of a single url
type visitedLink struct {
	// Link which was crawled to produce this
	link
	// if a fatal error occurred, it will be placed in this variable
	// If this is not nil, the link is to be considered NOT crawled and will NOT be crawled
	// XXX(ozum): this could be improved by implementing a retry mechanism based on the error
	// The same link could be added in visitedLink as a child with a nil link
	fatalErr error

	// Links inferred by visiting the link
	children []link
}

type job struct {
	requester       request.Requester
	baseURL         *url.URL
	parallelisation int

	siteMap SiteMap

	// visitor go routines aggregate children links through this channel
	collectorChan chan visitedLink
	// Visit demands are sent to this chan
	demandChan chan link

	errCount uint32

	filterPipeline []filter.URLFilter
}

func NewJob(parallelisation int, baseURL *url.URL, requester request.Requester) *job {
	filterPipeline := []filter.URLFilter{
		filter.NewDomainLockFilter(baseURL),
		filter.NewRobotsFilter(requester),
	}
	return &job{
		collectorChan:   make(chan visitedLink),
		parallelisation: parallelisation,
		demandChan:      make(chan link),
		baseURL:         baseURL,
		requester:       requester,
		siteMap:         make(SiteMap),
		filterPipeline:  filterPipeline,
	}
}

// sanitizeLink attempts to check if relative targets were given, and tries to convert them to
// absolute links
func (j *job) sanitizeLink(parent *url.URL, href string) string {
	if href == "/" {
		return parent.String()
	}
	if (len(href) > 0 && href[0] == '/') || !strings.HasPrefix(href, "http") {
		parentCpy := *parent
		parentCpy.Path = path.Join(parent.Path, href)

		return parentCpy.String()
	}

	return href
}

// inferLinks finds a tags from an html content and generate links
func (j *job) inferLinks(parent *url.URL, reader io.Reader) ([]link, error) {
	root, err := html.Parse(reader)
	if err != nil {
		return nil, errors.Wrap(err, "error while parsing html")
	}

	links := make([]link, 0, 10)
	nodes := []*html.Node{root}
	indice := 0
	for indice < len(nodes) {
		n := nodes[indice]
		if n.DataAtom == atom.A {
			for _, a := range n.Attr {
				if a.Key == "href" {
					// If the first character is a # ignore the link as it is not a target to go for
					if len(a.Val) > 0 && a.Val[0] == '#' {
						continue
					}
					parsed, err := url.Parse(j.sanitizeLink(parent, a.Val))
					if err != nil {
						logrus.WithError(err).Warn("Error while parsing url, skipping...")
					}
					links = append(links, link{
						parent: parent,
						url:    parsed,
					})
				}
			}
		}

		if n.NextSibling != nil {
			nodes = append(nodes, n.NextSibling)
		}
		if n.FirstChild != nil {
			nodes = append(nodes, n.FirstChild)
		}
		indice++
	}
	return links, nil
}

func (j *job) generateLinksFromResponse(parent *url.URL, resp *http.Response) ([]link, error) {
	return j.inferLinks(parent, resp.Body)
}

func (j *job) visitorWorker() {
	for {
		j.visitor()
	}
}

// visitor is meant to be run as a go routine which continuously expects link demands for crawling
func (j *job) visitor() {
	defer func() {
		if r := recover(); r != nil {
			logrus.WithField("stack", string(debug.Stack())).Error("job.visitor: RECOVERED FROM *PANIC*")
		}
	}()
	l := <-j.demandChan
	// If the link is in zero state, the channel is closed, we should return
	if l.parent == nil && l.url == nil {
		return
	}
	links := make([]link, 0, 10)
	var fatalError error
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	// Get triggers a go routine scheduling, hence higher number of go routines immensely
	// helps out with performance
	response, err := j.requester.Get(ctx, l.url)
	if err != nil {
		logrus.WithError(err).Error("job.visitor: error while visiting link")
		atomic.AddUint32(&j.errCount, 1)
		fatalError = err
	} else {
		switch response.StatusCode {
		// Bad status codes should not be counted in
		case http.StatusNotFound, http.StatusInternalServerError:
		case http.StatusPermanentRedirect, http.StatusTemporaryRedirect, http.StatusMovedPermanently:
			redirectedTo := response.Header["Location"]
			parsed, err := url.Parse(redirectedTo[0])
			if err != nil {
				fatalError = err
			}
			links = append(links, link{
				parent: l.url,
				url:    parsed,
			})
		default:
			newLinks, err := j.generateLinksFromResponse(l.url, response)
			if err != nil {
				atomic.AddUint32(&j.errCount, 1)
				logrus.WithError(err).Error("job.visitor: error while generating links")
				fatalError = err
			} else {
				// Make sure the links are unique, hence us a set
				m := make(map[string]struct{})
				for _, nl := range newLinks {
					_, ok := m[nl.url.String()]
					if !ok {
						//fmt.Printf("Generated link : %s\n", nl.url.String())
						links = append(links, nl)
						m[nl.url.String()] = struct{}{}
					}
				}
			}
		}
	}

	// Cancel the context at this point as we are done with the request
	cancel()

	j.collectorChan <- visitedLink{
		fatalErr: fatalError,
		children: links,
		link:     l,
	}
}

// approveLink checks if a link is fit for crawling
// only then the link will be demanded to be crawled
func (j *job) approveLink(l link) bool {
	// If the link was visited before, it should not be considered again
	_, ok := j.siteMap[l.url.String()]
	if ok {
		return false
	}

	// go through the pipeline
	for _, f := range j.filterPipeline {
		ok, err := f.Filter(l.url)
		if err != nil {
			logrus.WithError(err).Warn("job.approveLink: error while filtering")
			return false
		}
		if !ok {
			logrus.Debugf("url %s rejected by filter %s", l.url.String(), reflect.TypeOf(f))
			return false
		}
	}
	return true
}

// start executes the job and blocks until it is finished
// it does not return any errors despite the fact that errors are possible, because crawling in its nature
// a best effort execution. The error count is logged in the end. In the ideal world the number of errors could
// be checked for a threshold and the crawling could be failed.
func (j *job) Start() SiteMap {
	// Create as many go routines as the parallelisation factor
	for i := 0; i < j.parallelisation; i++ {
		go j.visitorWorker()
	}

	links := make([]link, 0, 200)
	// Add the base url as the first link
	links = append(links, link{
		parent: nil,
		url:    j.baseURL,
	})
	indice := 0
	for indice < len(links) {
		// If we are at the end of the slice, we might not be able to pick up the number of
		// links we want, so we should use a minimum op to decide
		step := minInt(j.parallelisation, len(links)-indice)
		linksToProcess := links[indice : indice+step]

		// Some links will not be approved, meaning the number of links we send of to the visitors
		// might be smaller than the step. We need to make sure we expect equal number of visitedLink
		// messages back from visitors to avoid any deadlocks. That is why numberOfApprovedLinks is used
		numberOfApprovedLinks := 0
		for _, l := range linksToProcess {
			if j.approveLink(l) {
				fmt.Printf("Approved link : %s\n", l.url.String())
				j.demandChan <- l
				numberOfApprovedLinks++
			}
		}

		// We collect the results
		for i := 0; i < numberOfApprovedLinks; i++ {
			p := <-j.collectorChan
			// If error is nil we should process
			if p.fatalErr == nil {
				// If parent is not nil, we should make sure we add the visitedLink to children of its parent
				// in the site-map
				if p.link.parent != nil {
					children, ok := j.siteMap[p.link.parent.String()]
					if !ok {
						children = make([]link, 0)
					}
					children = append(children, p.link)
					j.siteMap[p.link.parent.String()] = children
				}
				// We need to create a 0 children site-map entry for this visitedLink to avoid duplicates
				j.siteMap[p.link.url.String()] = make([]link, 0)
				// Add the children to the link to carry on the processing
				links = append(links, p.children...)
			}
		}

		indice += step
	}

	// Close the demand channels to avoid hanging go routines
	close(j.demandChan)

	fmt.Printf("Job finished with %d errors, having crawled %d links\n", j.errCount, len(links))

	return j.siteMap
}
