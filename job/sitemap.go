package job

import (
	"fmt"
	"io"
	"net/url"
	"os"
)

type SiteMap map[string][]link

// print prints the site-map in a directory-tree like manner recursively
// Undoubtedly a non-recursive approach is more efficient to avoid potential stack overflow
// But recursion provides simpler code
func (s SiteMap) print(startUrl *url.URL, initialSpaceFactor, spaceStep int, writer io.Writer) error {
	for i := 0; i < initialSpaceFactor; i++ {
		if i == initialSpaceFactor-spaceStep {
			_, err := fmt.Fprint(writer, "-")
			if err != nil {
				return err
			}
		} else {
			_, err := fmt.Fprint(writer, " ")
			if err != nil {
				return err
			}
		}
	}
	_, err := fmt.Fprintf(writer, "%s\n", startUrl.String())
	if err != nil {
		return err
	}
	children := s[startUrl.String()]
	for _, c := range children {
		err := s.print(c.url, initialSpaceFactor+spaceStep, spaceStep, writer)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s SiteMap) PrintStdOut(startUrl *url.URL, initialSpaceFactor int, spaceStep int) error {
	return s.print(startUrl, initialSpaceFactor, spaceStep, os.Stdout)
}
