package job

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func getURL(t *testing.T, u string) *url.URL {
	parsed, err := url.Parse(u)
	assert.Nil(t, err)
	return parsed
}

func TestSiteMap(t *testing.T) {
	out := `initial
-                   c1
                    -                   c1-1
                    -                   c1-2
-                   c2
                    -                   c2-1
                    -                   c2-2
                    -                   c2-3
-                   c3
`

	sm := make(SiteMap)
	sm["initial"] = []link{
		{
			url: getURL(t, "c1"),
		},
		{
			url: getURL(t, "c2"),
		},
		{
			url: getURL(t, "c3"),
		},
	}

	sm["c1"] = []link{
		{
			url: getURL(t, "c1-1"),
		},
		{
			url: getURL(t, "c1-2"),
		},
	}

	sm["c2"] = []link{
		{
			url: getURL(t, "c2-1"),
		},
		{
			url: getURL(t, "c2-2"),
		},
		{
			url: getURL(t, "c2-3"),
		},
	}

	buffer := bytes.NewBuffer([]byte{})
	err := sm.print(getURL(t, "initial"), 0, 20, buffer)
	assert.Nil(t, err)

	assert.Equal(t, out, buffer.String())
}
