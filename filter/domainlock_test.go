package filter

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestDomainLockFilter_WithoutWWW_Succeeds(t *testing.T) {
	parsed, err := url.Parse("https://www.monzo.com")
	assert.Nil(t, err)
	f := NewDomainLockFilter(parsed)

	toTest, err := url.Parse("https://monzo.com")
	assert.Nil(t, err)
	b, err := f.Filter(toTest)
	assert.Nil(t, err)

	assert.False(t, b)
}
