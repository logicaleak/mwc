package filter

import (
	"context"
	"fmt"
	"github.com/temoto/robotstxt"
	"io/ioutil"
	"mwc/request"
	"net/url"
	"time"
)

// robotsUrlFilter retrieves the robots.txt file of a url and tries to avoid
// unwanted visits
type robotsUrlFilter struct {
	robotsData *robotstxt.RobotsData
	requester  request.Requester
}

func NewRobotsFilter(requester request.Requester) URLFilter {
	return &robotsUrlFilter{
		requester: requester,
	}
}

func (r *robotsUrlFilter) Filter(url *url.URL) (bool, error) {
	// If data was already fetched, avoid getting it again
	if r.robotsData == nil {
		targetDomain := fmt.Sprintf("%s://%s/robots.txt", url.Scheme, url.Host)
		urlTargetDomain, err := url.Parse(targetDomain)
		if err != nil {
			return false, err
		}
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
		defer cancel()
		resp, err := r.requester.Get(ctx, urlTargetDomain)
		if err != nil {
			return false, err
		}
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return false, nil
		}
		data, err := robotstxt.FromBytes(bytes)
		if err != nil {
			return false, err
		}
		r.robotsData = data
	}

	return r.robotsData.TestAgent(url.Path, "*"), nil
}
