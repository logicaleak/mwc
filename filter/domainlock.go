package filter

import (
	"net/url"
)

type domainLockFilter struct {
	host string
}

func NewDomainLockFilter(base *url.URL) URLFilter {
	return &domainLockFilter{
		host: base.Host,
	}
}

func (d *domainLockFilter) Filter(url *url.URL) (bool, error) {
	return url.Host == d.host, nil
}
