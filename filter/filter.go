package filter

import "net/url"

// URLFilter is an interface which accepts a url and returns a
// filter decision
type URLFilter interface {
	Filter(url *url.URL) (bool, error)
}
