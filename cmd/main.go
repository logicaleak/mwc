package main

import (
	"flag"
	"fmt"
	"mwc"
	"time"
)

func main() {
	urlPtr := flag.String("url", "https://www.monzo.com", "url to be crawled")
	parallelisationInt := flag.Int("parallelisation", 100, "number of go routines to run")
	initialSpaceFactor := flag.Int("initial-space-factor", 0, "Offset from the left site for site-map printing")
	spaceStepInt := flag.Int("space-step", 10, "Number of horizontal spaces between parent and children")
	flag.Parse()

	crawler := mwc.NewCrawler()
	n := time.Now()
	err := crawler.Crawl(*urlPtr, &mwc.Config{
		Parallelisation:    *parallelisationInt,
		InitialSpaceFactor: *initialSpaceFactor,
		SpaceStep:          *spaceStepInt,
	})
	if err != nil {
		panic(err)
	}
	fmt.Printf("Crawling took %s\n", time.Now().Sub(n))
}
