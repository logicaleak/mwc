package mwc

import (
	"mwc/job"
	"mwc/request"
	"net/url"
)

type Config struct {
	// Number of go routines to run during the crawling
	Parallelisation    int
	// Initial space offset for printing
	InitialSpaceFactor int
	// Number of spaces between parent and childdren
	SpaceStep          int
}

// crawler given a baseURL and a configuration
// crawls a url and prints the sitemap
type crawler struct {
}

func NewCrawler() *crawler {
	return &crawler{}
}

func (c *crawler) Crawl(baseURL string, cfg *Config) error {
	parsed, err := url.Parse(baseURL)
	if err != nil {
		return err
	}

	requester := request.NewRequester()
	sm := job.NewJob(cfg.Parallelisation, parsed, requester).Start()
	return sm.PrintStdOut(parsed, cfg.InitialSpaceFactor, cfg.SpaceStep)
}
