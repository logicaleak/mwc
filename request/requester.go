package request

import (
	"context"
	"net/http"
	"net/url"
)

// Requester is an interface which may have various implementations to
// make a Get request. For example defaultRequester blandly makes a request
// with a context object, without caring about the returned status code. We
// could write another Requester who checks for status code 429 and start throttling and retry
type Requester interface {
	Get(ctx context.Context, url *url.URL) (*http.Response, error)
}

// Agent is the user agent used by the crawler.
const Agent = "Mozilla/5.0 (compatible; MonzoCrawler; "

// Requester is the common request interface for all
// the functionality contained in the web-crawler
type defaultRequester struct {
}

func NewRequester() Requester {
	return &defaultRequester{}
}

func (d *defaultRequester) Get(ctx context.Context, url *url.URL) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return nil, err
	}
	d.addHeaders(req.Header)
	req = req.WithContext(ctx)

	c := http.Client{}
	return c.Do(req)
}

func (d *defaultRequester) addHeaders(h http.Header) {
	h.Add("Accept", "text/html")
	h.Add("Accept-Charset", "utf-8")
	h.Add("Cache-Control", "no-cache")
	h.Add("User-Agent", Agent)
}
